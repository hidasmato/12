const timeMax = 10000;
async function start() {
    new Promise((resolve, reject) => {
        const timer = timeMax * Math.random();
        setTimeout(() => {
            resolve("F1 sucsess arter " + Math.floor(timer) + "ms");
        }, timer);
    }).then((message) =>
        console.log(message)
    )

    let promisFunc = new Promise((resolve, reject) => {
        const timer = timeMax * Math.random();
        setTimeout(() => {
            resolve("F2 sucsess arter " + Math.floor(timer) + "ms");
        }, timer);
    })
    console.log(await promisFunc);
}

start();